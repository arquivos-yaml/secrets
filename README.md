### SECRETS ###

Objeto que contém uma pequena quantidade informação sensível, como senhas, tokens ou chaves, dever ser passado criptografado em base64.

Neste exemplos estamos criando uma secret dentro de um pod NGINX.